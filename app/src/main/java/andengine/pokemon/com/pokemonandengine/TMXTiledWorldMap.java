package andengine.pokemon.com.pokemonandengine;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.Constants;
import org.andengine.util.debug.Debug;

import android.graphics.Point;
import android.opengl.GLES20;
import android.view.Display;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

/**
 * Created by anibal on 21/08/15.
 */
public class TMXTiledWorldMap extends SimpleBaseGameActivity  {
    // ===========================================================
    // Constants
    // ===========================================================

    private static int CAMERA_WIDTH = 800;
    private static int CAMERA_HEIGHT = 480;
    private static int FRAME_DURATION = 150;

    // ===========================================================
    // Fields
    // ===========================================================

    private BoundCamera mBoundChaseCamera;

    private TiledTextureRegion mPlayerTextureRegion;
    private TMXTiledMap mTMXTiledMap;
    protected int mCactusCount;

    private BitmapTextureAtlas mBitmapTextureAtlas;

    private BitmapTextureAtlas mOnScreenControlTexture;
    private ITextureRegion mOnScreenControlBaseTextureRegion;
    private ITextureRegion mOnScreenControlKnobTextureRegion;

    private DigitalOnScreenControl mDigitalOnScreenControl;

    private PlayerDirection playerDirection = PlayerDirection.DOWN;

    private PhysicsWorld mPhysicsWorld;
    private boolean mCollition = false;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    private void setmDigitalOnScreenControl(final AnimatedSprite player, final PhysicsHandler physicsHandler){

        this.mDigitalOnScreenControl = new DigitalOnScreenControl(0, CAMERA_HEIGHT - this.mOnScreenControlBaseTextureRegion.getHeight(), this.mBoundChaseCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, this.getVertexBufferObjectManager(), new BaseOnScreenControl.IOnScreenControlListener() {
            @Override
            public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, final float pValueX, final float pValueY) {
                if (pValueY == 1){
                    // Up
                    if (playerDirection != PlayerDirection.UP){
                        player.animate(new long[]{FRAME_DURATION, FRAME_DURATION, FRAME_DURATION, FRAME_DURATION}, 0, 3, true);
                        playerDirection = PlayerDirection.UP;
                    }
                }else if (pValueY == -1){
                    // Down
                    if (playerDirection != PlayerDirection.DOWN){
                        player.animate(new long[]{FRAME_DURATION, FRAME_DURATION, FRAME_DURATION, FRAME_DURATION}, 12, 15, true);
                        playerDirection = PlayerDirection.DOWN;
                    }
                }else if (pValueX == -1){
                    // Left
                    if (playerDirection != PlayerDirection.LEFT){
                        player.animate(new long[]{FRAME_DURATION, FRAME_DURATION, FRAME_DURATION, FRAME_DURATION}, 4, 7, true);
                        playerDirection = PlayerDirection.LEFT;
                    }
                }else if (pValueX == 1){
                    // Right
                    if (playerDirection != PlayerDirection.RIGHT){
                        player.animate(new long[]{FRAME_DURATION, FRAME_DURATION, FRAME_DURATION, FRAME_DURATION}, 8, 11, true);
                        playerDirection = PlayerDirection.RIGHT;
                    }
                }else{
                    if (player.isAnimationRunning()){
                        player.stopAnimation();
                        playerDirection = PlayerDirection.NONE;
                    }
                }
                //Debug.e("X:"+pValueX+" Y"+pValueY);
                if(!mCollition) {
                    physicsHandler.setVelocity(pValueX * 100, pValueY * 100);
                }else{
                    physicsHandler.setVelocity(0,0);
                    player.setPosition(player.getX()-pValueX*12, player.getY()-pValueY *31);
                    mCollition = false;
                }
            }
        });
        this.mDigitalOnScreenControl.getControlBase().setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        this.mDigitalOnScreenControl.getControlBase().setAlpha(0.5f);
        this.mDigitalOnScreenControl.getControlBase().setScaleCenter(0, 128);
        this.mDigitalOnScreenControl.getControlBase().setScale(1.25f);
        this.mDigitalOnScreenControl.getControlKnob().setScale(1.25f);
        this.mDigitalOnScreenControl.refreshControlKnobPosition();
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public EngineOptions onCreateEngineOptions() {
        // getting the device's screen size

        Point screenSize = getScreenSizeByRatio();
        CAMERA_WIDTH = screenSize.x;
        CAMERA_HEIGHT = screenSize.y;


        Toast.makeText(this, "The tile the player is walking on will be highlighted.", Toast.LENGTH_LONG).show();

        Toast.makeText(this, "Also try tapping this AnalogOnScreenControl!", Toast.LENGTH_LONG).show();

        // Create physics world
        this.mPhysicsWorld = new FixedStepPhysicsWorld(30, new Vector2(0, 0), false, 8, 1);

        this.mBoundChaseCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mBoundChaseCamera);
    }

    @Override
    public void onCreateResources() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        this.mBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 256, 256, TextureOptions.DEFAULT);
        this.mPlayerTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas, this, "025_0.png", 0, 0, 4, 4);
        this.mBitmapTextureAtlas.load();

        this.mOnScreenControlTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 128, TextureOptions.BILINEAR);
        this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_base.png", 0, 0);
        this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_knob.png", 128, 0);
        this.mOnScreenControlTexture.load();
    }

    @Override
    public Scene onCreateScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());

        final Scene scene = new Scene();
        scene.registerUpdateHandler(mPhysicsWorld);

		/* Calculate the coordinates for the face, so its centered on the camera. */
        final float centerX = (CAMERA_WIDTH - this.mPlayerTextureRegion.getWidth()) / 2;
        final float centerY = (CAMERA_HEIGHT - this.mPlayerTextureRegion.getHeight()) / 2;

        try {
            final TMXLoader tmxLoader = new TMXLoader(this.getAssets(), this.mEngine.getTextureManager(), TextureOptions.BILINEAR_PREMULTIPLYALPHA, this.getVertexBufferObjectManager(), new ITMXTilePropertiesListener() {
                @Override
                public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {
					/* We are going to count the tiles that have the property "cactus=true" set. */
                    if(pTMXTileProperties.containsTMXProperty("cactus", "true")) {
                        TMXTiledWorldMap.this.mCactusCount++;
                    }
                }
            });
            this.mTMXTiledMap = tmxLoader.loadFromAsset("desert.tmx");

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(TMXTiledWorldMap.this, "Cactus count in this TMXTiledMap: " + TMXTiledWorldMap.this.mCactusCount, Toast.LENGTH_LONG).show();
                }
            });
        } catch (final TMXLoadException e) {
            Debug.e(e);
        }

        final TMXLayer tmxLayer = this.mTMXTiledMap.getTMXLayers().get(0);
        scene.attachChild(tmxLayer);

		/* Make the camera not exceed the bounds of the TMXEntity. */
        this.mBoundChaseCamera.setBounds(0, 0, tmxLayer.getHeight(), tmxLayer.getWidth());
        this.mBoundChaseCamera.setBoundsEnabled(true);

		/* Create the sprite and add it to the scene. */
        final AnimatedSprite player = new AnimatedSprite(centerX, centerY, this.mPlayerTextureRegion, this.getVertexBufferObjectManager());
        final FixtureDef playerFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 0.5f);
        PhysicsFactory.createBoxBody(mPhysicsWorld, centerX, centerY, player.getWidthScaled(), player.getHeightScaled(), BodyDef.BodyType.DynamicBody, playerFixtureDef);
        this.mBoundChaseCamera.setChaseEntity(player);

        /* Create Physics Handler. */
        final PhysicsHandler physicsHandler = new PhysicsHandler(player);
        player.registerUpdateHandler(physicsHandler);

        /* Set D-Pad and add it to the scene. */
        setmDigitalOnScreenControl(player, physicsHandler);
        scene.setChildScene(this.mDigitalOnScreenControl);


		/* Now we are going to create a rectangle that will  always highlight the tile below the feet of the pEntity. */
        final Rectangle currentTileRectangle = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight(), this.getVertexBufferObjectManager());
        currentTileRectangle.setColor(1, 0, 0, 0.25f);
        scene.attachChild(currentTileRectangle);

        scene.registerUpdateHandler(new IUpdateHandler() {

            @Override
            public void reset() { }

            @Override
            public void onUpdate(final float pSecondsElapsed) {
				// Get the scene-coordinates of the players feet.
                final float[] playerFootCordinates = player.convertLocalToSceneCoordinates(12, 31);

				// Get the tile the feet of the player are currently waking on.
                final TMXTile tmxTile = tmxLayer.getTMXTileAt(playerFootCordinates[Constants.VERTEX_INDEX_X], playerFootCordinates[Constants.VERTEX_INDEX_Y]);
                if(tmxTile != null) {
                    // tmxTile.setTextureRegion(null); <-- Rubber-style removing of tiles =D
                    try {
                        if (tmxTile.getTMXTileProperties(mTMXTiledMap).containsTMXProperty("wall", "true")) {
                            Debug.e("A WALL");
                            player.stopAnimation();
                            mCollition = true;
                        }else {
                            mCollition = false;
                        }
                    }catch (Exception ex){ }
                    //if(tmxTile.containsTMXProperty("wall","true"))
                    currentTileRectangle.setPosition(tmxTile.getTileX(), tmxTile.getTileY());
                }
            }
        });
        scene.attachChild(player);

        return scene;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public Point getScreenSizeByRatio(){
        final Display display = getWindowManager().getDefaultDisplay();

        double sw = display.getWidth();
        double sh = display.getHeight();
        double ratio = sw / sh;
        if (sw < sh) ratio = sh/sw;

        Point screenPoints = new Point();
        if (ratio > 1.3 && ratio < 1.4)
        {
            screenPoints.x = 680;
            screenPoints.y = 480;

        } else
        {
            screenPoints.x = 800;
            screenPoints.y = 480;

        }

        return screenPoints;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private enum PlayerDirection{
        NONE,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}